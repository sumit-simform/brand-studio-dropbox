// Module dependencies

const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
var exphbs = require('express-handlebars');
const cors = require('cors');
const config = require('./config');
const helper = require('./helper');
const mongoose = require('mongoose');


// MongoDB Initialization
mongoose.connect(config.DB_URI, { useNewUrlParser: true }, ((err) => {
    if (err) {
        console.log('Could not connect to MongoDB', err);
    } else {
        mongoose.set('debug', false);
    }
}));

const models = helper.getGlobbedPaths(config.dbModels);
models.forEach((modelPath) => {
    require(path.resolve(modelPath));
});

// ExpressJS Initialization
const app = express();
app.use(bodyParser.json());

app.engine('.hbs', exphbs({ extname: '.hbs' }));
app.set('view engine', '.hbs');
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(cors());

app.use(express.static('public'));

app.get('*', function (req, res) {
   
    res.sendFile(path.join(__dirname + '/public/index.html'));
})
const routes = helper.getGlobbedPaths(config.serverRoutes);
routes.forEach((routePath) => {
    require(path.resolve(routePath))(app);
});

// NodeJS Server Initialization
app.listen(config.apiPort, () => {
    console.log(`App is running on port ${config.apiPort}`);
});
