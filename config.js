module.exports = {
    DBX_API_DOMAIN: 'https://api.dropboxapi.com',
    DBX_OAUTH_DOMAIN: 'https://www.dropbox.com',
    DBX_APP_KEY: 'eb2ft35xk7hu2kw',
    DBX_APP_SECRET: 'gx2vzb4y3u2xd66',
    OAUTH_REDIRECT_URL: "https://demo.simformsolutions.com:40300/oauthredirect",
    serverRoutes: 'modules/*/*.route.js',
    dbModels: 'modules/*/*.model.js',
    apiPort: 40300,
    DBX_LIST_FOLDER_PATH: '/2/files/list_folder',
    DBX_LIST_FOLDER_CONTINUE_PATH: '/2/files/list_folder/continue',
    DBX_GET_TEMPORARY_LINK_PATH: '/2/files/get_temporary_link',
    DBX_GET_LATEST_CURSOR: '/2/files/list_folder/get_latest_cursor',
    DB_URI: 'mongodb://localhost:27017/brand-studio'
}
