const dropboxV2Api = require('dropbox-v2-api');
const config = require('./../../config');
const rp = require('request-promise');
const fs = require('fs');
const https = require('https');
const fsPath = require('fs-path');
const mongoose = require('mongoose');
const Dropbox = mongoose.model('dropbox');
const async = require('async');
let cursor;

exports.getAceessToken = async (req, res) => {
    const dropbox = dropboxV2Api.authenticate({
        client_id: req.body.clientId,
        client_secret: req.body.clientSecret,
        redirect_uri: config.OAUTH_REDIRECT_URL
    });
    const authUrl = dropbox.generateAuthUrl();
    return res.status(200).json({ 'authUrl': authUrl });
};

exports.getTokenFromTheCode = async (req, res) => {
    res.render('call-button');
};

exports.sendToken = async (req, res) => {
    const dropbox = dropboxV2Api.authenticate({
        client_id: req.body.clientId,
        client_secret: req.body.clientSecret,
        redirect_uri: config.OAUTH_REDIRECT_URL
    });
    dropbox.getToken(req.body.code, (err, response) => {
        if (response && response.access_token) {
            // save all data to db 
            let newDropbox = {
                key: req.body.clientId,
                secret: req.body.clientSecret,
                token: response.access_token,
                cursor: ''
            };
            // if there is no dropbox data then save
            (async () => {
                // save post as released
                let dropboxs = await Dropbox.find();
                if (!dropboxs.length) {
                    const dp = new Dropbox(newDropbox);
                    dp.save();
                }
            })();
            return res.status(200).json({ 'access_token': response.access_token });
        } else if (response && response.error) {
            return res.status(500).json({ 'Error': response.error });
        }
    });
};


exports.getDataList = async (req, res) => {
    let folders = [];
    if (req.body.folderName1) {
        folders.push(req.body.folderName1);
        // download all files of particular folder
        // await downloadFiles(req.body.access_token, req.body.folderName1);
        const dir = './public/media/' + req.body.folderName1;
        if (!fs.existsSync(dir)) {
            await downloadFiles(req.body.access_token, req.body.folderName1);
        }
    }
    if (req.body.folderName2) {
        folders.push(req.body.folderName2);
        // download all files of particular folder
        // check if folder is not exists then download files 
        const dir = './public/media/' + req.body.folderName2;
        if (!fs.existsSync(dir)) {
            await downloadFiles(req.body.access_token, req.body.folderName2);
        }
    }
    // get token 
    getFilePaths(folders).then((data) => {
        // filter the images and video
        return res.render('gallery', { imgs: data.imagePaths, videos: data.videoPaths, layout: false });
    });


};

exports.webHookVerification = async (req, res) => {
    return res.send(req.query.challenge);
};

exports.webHookApiCall = async (req, res) => {
    // sync the data of dropbox here 
    // get the dropbox files according to the body
    try {
        // get token 
        let dropboxData = await Dropbox.findOne();
        if (cursor) {
            let getFilesContinue = {
                url: config.DBX_API_DOMAIN + config.DBX_LIST_FOLDER_CONTINUE_PATH,
                headers: { "Authorization": "Bearer " + dropboxData.token },
                method: 'POST',
                json: true,
                body: {
                    "cursor": cursor
                }
            }
            let result = await rp(getFilesContinue);
            cursor = result.cursor;
            if (result.entries && result.entries.length) {
                // get the files to store in the local directory
                let message = await downloadFilesFromEntries(dropboxData.token, result.entries);
                return res.send({ 'message': message });
            } else {
                return res.send({ 'message': 'sync success' });
            }

        } else {
            let options = {
                url: config.DBX_API_DOMAIN + config.DBX_GET_LATEST_CURSOR,
                headers: { "Authorization": "Bearer " + dropboxData.token },
                method: 'POST',
                json: true,
                body: {
                    "path": "",
                    "recursive": true,
                    "include_media_info": false,
                    "include_deleted": false,
                    "include_has_explicit_shared_members": false,
                    "include_mounted_folders": true
                }
            }
            let result = await rp(options);
            let getFilesContinue = {
                url: config.DBX_API_DOMAIN + config.DBX_LIST_FOLDER_CONTINUE_PATH,
                headers: { "Authorization": "Bearer " + dropboxData.token },
                method: 'POST',
                json: true,
                body: {
                    "cursor": result.cursor
                }
            }
            let result1 = await rp(getFilesContinue);
            cursor = result1.cursor;
            if (result1.entries && result1.entries.length) {
                // get the files to store in the local directory
                let message = await downloadFilesFromEntries(dropboxData.token, result1.entries);
                return res.send({ 'message': message });
            } else {
                return res.send({ 'message': 'sync success' });
            }
        }
    } catch (error) {
        return new Error('error listing folder. ' + error.message);
    }
};


async function getLinksAsync(token, folderArray) {
    let result = {};
    let imagePaths = [];
    let videoPAths = [];
    if (folderArray.length) {


        await Promise.all(folderArray.map(folder => listImagePathsAsync(token, folder))).then((data) => {
            data.map(result => {
                imagePaths = [...imagePaths, ...result.paths];
                videoPAths = [...videoPAths, ...result.videoPaths];
            });
        })
    } else {
        result = await listImagePathsAsync(token, '');
        imagePaths = result.paths.concat();
        videoPAths = result.videoPaths.concat();
    }
    //Get a temporary link for each of those paths returned
    let temporaryLinkResults = await getTemporaryLinksForPathsAsync(token, imagePaths);
    let temporaryVideoLinkResults = await getTemporaryLinksForPathsAsync(token, videoPAths);

    //Construct a new array only with the link field
    let temporaryLinks = temporaryLinkResults.map(function (entry) {
        return entry.link;
    });

    let temporaryVideoLinks = temporaryVideoLinkResults.map(function (entry) {
        return entry.link;
    });

    return { imagePaths: temporaryLinks, videoPaths: temporaryVideoLinks };
}


async function listImagePathsAsync(token, path) {
    let options = {
        url: config.DBX_API_DOMAIN + config.DBX_LIST_FOLDER_PATH,
        headers: { "Authorization": "Bearer " + token },
        method: 'POST',
        json: true,
        body: { "path": '/' + path }
    }

    try {
        //Make request to Dropbox to get list of files
        let result = await rp(options);
        //Filter response to images only
        let entriesFiltered = result.entries.filter(function (entry) {
            return entry.path_lower.search(/\.(gif|jpg|jpeg|tiff|png)$/i) > -1;
        });

        let entriesVideoFiltered = result.entries.filter(function (entry) {
            return entry.path_lower.search(/\.(mp4|webm|fly|avi|3gp|mov)$/i) > -1;
        });

        //Get an array from the entries with only the path_lower fields
        let paths = entriesFiltered.map(function (entry) {
            return entry.path_lower;
        });

        let videoPaths = entriesVideoFiltered.map(function (entry) {
            return entry.path_lower;
        });


        //return a cursor only if there are more files in the current folder
        let response = {};
        response.paths = paths;
        response.videoPaths = videoPaths;
        if (result.hasmore) response.cursor = result.cursor;
        return response;

    } catch (error) {
        return new Error('error listing folder. ' + error.message);
    }
}

function getTemporaryLinksForPathsAsync(token, paths) {

    let promises = [];
    let options = {
        url: config.DBX_API_DOMAIN + config.DBX_GET_TEMPORARY_LINK_PATH,
        headers: { "Authorization": "Bearer " + token },
        method: 'POST',
        json: true
    }
    //Create a promise for each path and push it to an array of promises
    paths.forEach((path_lower) => {
        options.body = { "path": path_lower };
        promises.push(rp(options));
    });

    //returns a promise that fullfills once all the promises in the array complete or one fails
    return Promise.all(promises);
}

async function downloadFiles(token, folderName) {
    try {
        // download files in local according to token
        const result = await listImagePathsAsync(token, folderName);
        const images = await getTemporaryLinksForPathsAsync(token, result.paths);
        const videos = await getTemporaryLinksForPathsAsync(token, result.videoPaths);
        const files = [...images, ...videos];
        const dir = './public/media/' + folderName;
        if (!fs.existsSync('./public/media')) {
            fs.mkdirSync('./public/media');
        }
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
        const ts = files.map(file => {
            const createdFile = fs.createWriteStream(dir + '/' + file.metadata.name);
            const request = https.get(file.link, function (response) {
                response.pipe(createdFile);
                return request;
            });
        });
        return res.status(200).json({ 'message': 'Files Successfully Downloaded!!!!' });
    } catch (error) {
        return new Error('error listing folder. ' + error.message);
    }
};

async function downloadFilesFromEntries(token, filesArray) {
    let paths = filesArray.map(file => file.path_lower);
    const files = await getTemporaryLinksForPathsAsync(token, paths);
    files.map(file => {
        fsPath.writeFile('./public/media' + file.metadata.path_display, 'content', function (err) {
            if (err) {
                throw err;
            } else {
                const createdFile = fs.createWriteStream('./public/media' + file.metadata.path_display);
                const request = https.get(file.link, function (response) {
                    response.pipe(createdFile);
                    return request;
                });
            }
        });
    });
    return 'Success';
}

async function getFilePaths(folderArray, callbackAwait) {
    return new Promise(resolve => {
        let imagePaths = [];
        let videoPAths = [];
        async.eachLimit(folderArray, 1, function (folder, callback) {
            // Perform operation on file here.
            const testFolder = './public/media/' + folder + '/';
            fs.readdir(testFolder, (err, files) => {
                if (files && files.length) {
                    const modifiedFilesPaths = files.map(file => {
                        return 'media/' + folder + '/' + file;
                    });
                    let entriesFiltered = modifiedFilesPaths.filter(function (entry) {
                        return entry.search(/\.(gif|jpg|jpeg|tiff|png)$/i) > -1;
                    });
                    let entriesVideoFiltered = modifiedFilesPaths.filter(function (entry) {
                        return entry.search(/\.(mp4|webm|fly|avi|3gp|mov)$/i) > -1;
                    });
                    imagePaths = [...imagePaths, ...entriesFiltered];
                    videoPAths = [...videoPAths, ...entriesVideoFiltered];
                    callback();
                } else {
                    callback(err);
                }
            });
        }, function (err) {
            // if any of the file processing produced an error, err would equal that error
            if (err) {
                // One of the iterations produced an error.
                // All processing will now stop.
                console.log('A file failed to process', err);
            } else {
                resolve({ imagePaths: imagePaths, videoPaths: videoPAths });
            }
        });
    });
}