const mongoose = require('mongoose');
const { Schema } = mongoose;

const DropboxSchema = new Schema({
    key: { type: String },
    secret: { type: String },
    token: { type: String },
    cursor: { type: String }
});

mongoose.model('dropbox', DropboxSchema);
