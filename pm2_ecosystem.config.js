module.exports = {
    'apps': [{
        'name': 'brand-studio-dropbox',
        'script': 'server.js',
        'env': {
            'NODE_ENV': 'development'
        },
        'env_production': {
            'NODE_ENV': 'production'
        }
    }]
};