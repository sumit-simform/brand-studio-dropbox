'use strict';

const dropbox = require('./dropbox.controller');

module.exports = (app) => {
    app.route('/api/auth').post(dropbox.getAceessToken);
    app.route('/oauthredirect').get(dropbox.getTokenFromTheCode);
    app.route('/api/get/token').post(dropbox.sendToken);
    app.route('/api/list/data').post(dropbox.getDataList);
    app.route('/api/web-hook').get(dropbox.webHookVerification);
    app.route('/api/web-hook').post(dropbox.webHookApiCall);
};
